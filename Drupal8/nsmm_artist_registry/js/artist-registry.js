(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.artistRegistry = {
    attach: function(context) {
      // Meant for dialog.
      $('body').once('ar-enabled').addClass('ar-enabled');

      $(context).find('.paragraph--type--artist-registry-featured').once('slick-carousel').each(function() {
        var $carousel = $('.featured-carousel', this);

        // Add a Slick Carousel if there is more than one.
        if ($carousel.children().length > 1) {
          $carousel.slick({
            'arrows': false,
            'dots': true,
            'appendDots': $('.featured-nav', this),
            'adaptiveHeight': true,
            'autoplay': true,
            'autoplaySpeed': 9000
          });
        }
      });
    }
  }

}(jQuery, Drupal, drupalSettings));
