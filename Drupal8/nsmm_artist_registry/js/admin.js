(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.artistRegistryAdmin = {
    attach: function(context) {
      $('.node-art-form, .node-art-edit-form', context).once('art-form-states').each(function() {
        // Retrieve the form.
        var $form = $(this);

        $(':input[name="field_artwork_type"]', this).change(function(e) {
          var current = $(this).val();

          $.each(drupalSettings.nsmmArtistRegistry.states, function(field, value) {
            // Show the currently selected element.
            if (value == current) {
              $form.find(field).show();
            }
            else {
              $form.find(field).hide();
            }
          });
        }).trigger('change');
      });
    }
  }

}(jQuery, Drupal, drupalSettings));
