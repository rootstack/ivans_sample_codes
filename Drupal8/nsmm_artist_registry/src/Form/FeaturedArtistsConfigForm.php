<?php

namespace Drupal\nsmm_artist_registry\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Template\TwigEnvironment;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class FeaturedArtistsConfigForm.
 */
class FeaturedArtistsConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  protected $cacheTags;

  /**
   * Constructs a new FeaturedArtistsConfigForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    CacheTagsInvalidator $cache_tags
    ) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->cacheTags = $cache_tags;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'nsmm_artist_registry.featuredartistsconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'featured_artists_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('nsmm_artist_registry.featuredartistsconfig');

    $profileIds = $config->get('profiles') ?? [];

    $form['featured_section'] = [
      '#type' => 'details',
      '#title' => $this->t('Featured Section'),
      '#description' => $this->t('Use this field to choose which artists
        to show in the Featured section. To add multiple values, select a name from the autocomplete and save your selection. After saving, an additional field will be available.'),
      '#open' => TRUE,
    ];

    $form['featured_section']['profiles'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $selected_profiles = $this->entityTypeManager->getStorage('profile')->loadMultiple($profileIds);

    $nextProfileDisplayTitle = TRUE;

    foreach ($profileIds as $profileId) {
      $form['featured_section']['profiles'][] = $this->createProfileField($nextProfileDisplayTitle, $selected_profiles[$profileId]);
      $nextProfileDisplayTitle = FALSE;
    }

    // Create an empty field.
    $form['featured_section']['profiles'][] = $this->createProfileField($nextProfileDisplayTitle);

    return parent::buildForm($form, $form_state);
  }

  /**
   * @param \Drupal\profile\Entity\Profile $defaultValue
   *   Profile entity.
   *
   * @return array
   *   Entity Autocomplete element with default value if provided.
   */
  private function createProfileField($showTitle = TRUE, $defaultValue = NULL) {

    return [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Featured Artist'),
      '#title_display' => $showTitle ? 'before' : 'hidden',
      '#target_type' => 'profile',
      '#selection_handler' => 'default:artist_profile_selection',
      '#selection_settings' => ['target_bundles' => ['artist_profile']],
      '#default_value' => $defaultValue,
      '#size' => 60,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $profiles = $form_state->getValue('profiles');

    foreach ($profiles as $key => $profile) {
      if (empty($profile)) {
        unset($profiles[$key]);
      }
    }

    // Load node object from /registry route and invalidate cache tags.
    $path = \Drupal::service('path.alias_manager')->getPathByAlias('/registry');
    if(preg_match('/node\/(\d+)/', $path, $matches)) {
      $node = $this->entityTypeManager->getStorage('node')->load($matches[1]);
      $this->cacheTags->invalidateTags($node->getCacheTags());
    }

    $this->config('nsmm_artist_registry.featuredartistsconfig')
      ->set('profiles', $profiles)
      ->save();
  }

}
