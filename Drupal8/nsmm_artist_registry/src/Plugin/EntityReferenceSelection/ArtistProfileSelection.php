<?php
namespace Drupal\nsmm_artist_registry\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\node\NodeInterface;

/**
 * Provides specific access control for the profile entity type.
 *
 * @EntityReferenceSelection(
 *   id = "default:artist_profile_selection",
 *   label = @Translation("Article profile selection"),
 *   entity_types = {"profile"},
 *   group = "default",
 *   weight = 1
 * )
 */
class ArtistProfileSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS', $nid = NULL) {
    $query = parent::buildEntityQuery($match, $match_operator);
    $match = preg_replace('/\s\s+/', ' ', $match);
    $items = explode(" ", trim($match));
    $groupOr = $query->orConditionGroup();
    $groupAnd1 = $query->andConditionGroup();
    $groupAnd2 = $query->andConditionGroup();
    if (count($items) < 2) {
      $groupOr->condition('field_ar_first_name',$items[0], $match_operator);
      $groupOr->condition('field_ar_last_name', $items[0], $match_operator);
      $query->condition($groupOr);
    }
    else {
      $groupAnd1->condition('field_ar_first_name',$items[0], $match_operator);
      $groupAnd1->condition('field_ar_last_name', $items[1], $match_operator);
      $groupAnd2->condition('field_ar_last_name',$items[0], $match_operator);
      $groupAnd2->condition('field_ar_first_name', $items[1], $match_operator);
      $groupOr->condition($groupAnd1);
      $groupOr->condition($groupAnd2);
      $query->condition($groupOr);
    }

    return $query;
  }

}
