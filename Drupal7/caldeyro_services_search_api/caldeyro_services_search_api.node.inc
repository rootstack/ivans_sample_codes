<?php

/**
 * Callback function for retrieve a node.
 */
function caldeyro_services_search_api_get_node($index, $lang, $path_alias, $filter) {
  $node = drupal_lookup_path('source', $path_alias, $lang);
  $arguments = explode('/', $node);
  $filter['language'] = $lang;
  if (!empty($arguments[1]) && is_numeric($arguments[1])) {
    $filter['nid'] = $arguments[1];
    $filter['status'] = 1;
    $items_per_page = 1;
    $page = 0;
    module_load_include('inc', 'caldeyro_services_search_api', 'caldeyro_services_search_api.resources');
    return caldeyro_services_search_api_resource_retrieve($index, '', $filter, '', '', $items_per_page, $page, FALSE);
  }
  return t('Not found the path alias');
}
