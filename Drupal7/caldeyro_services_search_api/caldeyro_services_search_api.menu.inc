<?php

/**
 * Callback function for retrieve menu info.
 */
function caldeyro_services_search_api_get_menu($lang, $menu_name) {
  $query = db_select('menu_links', 'ml');
  $query->fields('ml',
    array(
      'mlid', 'link_path', 'link_title', 'external', 'p1',
    )
  );
  $query->condition('ml.menu_name', $menu_name, '=');
  $query->condition('ml.language', $lang, '=');
  $query->condition('ml.hidden', 0, '=');
  $query->orderBy('ml.depth', 'ASC');
  $query->orderBy('ml.weight', 'ASC');
  $result = $query->execute()->fetchAllAssoc('mlid');
  $temp = array();
  foreach ($result as $key => $item) {
    $temp[$key]->link = $item;
    if (!$item->external) {
      $item->link_path = drupal_get_path_alias($item->link_path, $lang);
      unset($item->external);
    }
    unset($item->mlid);
  }
  foreach ($temp as $key => $item) {
    $parent = $item->link->p1;
    unset($item->link->p1);
    if ($parent == $key) {
      continue;
    }
    if (!isset($temp[$parent]->below)) {
      $temp[$parent]->below = new StdClass();
    }
    $temp[$parent]->below->{$key} = $temp[$key];
    unset($temp[$key]);
  }
  return $temp;
}
