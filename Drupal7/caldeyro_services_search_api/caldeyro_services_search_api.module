<?php

/**
 * @file
 * Module main hooks implementation.
 */

/**
 * Implements hook_services_search_api_result_alter().
 */
function caldeyro_services_search_api_services_search_api_result_alter(&$return, $query, $fields) {

  $facet_adapter = facetapi_adapter_load('search_api@' . $query->getIndex()->machine_name);

  $enabled_facets = $facet_adapter->getEnabledFacets('block');
  foreach ($enabled_facets as $key => $facet) {
    $processor = $facet_adapter->getProcessor($key);
    $build = $processor->getBuild();

    foreach ($build as $option_key => $option) {
      $services_facet_result[$key][$option_key]['_facet'] = $option;
    }
  }
  $return['facets'] = $services_facet_result;
}

/**
 * Implements hook_permission().
 */
function caldeyro_services_search_api_permission() {
  return array(
    'caldeyro_services_search_api_search_from_any_index' => array(
      'title' => t('Retrieve search result from any query'),
    ),
    'caldeyro_services_search_api_get_webforms_info' => array(
      'title' => t('Retrieve webform information'),
    ),
    'caldeyro_services_facets_api_get_facet_setting' => array(
      'title' => t('Retrieve settings for the facets'),
    ),
    'caldeyro_services_search_api_get_info_page' => array(
      'title' => t('Retrieve information from a page'),
    ),
    'caldeyro_services_search_api_get_info_node' => array(
      'title' => t('Retrieve information from a node'),
    ),
    'caldeyro_services_search_api_get_info_menu' => array(
      'title' => t('Retrieve information from a menu'),
    ),
    'caldeyro_services_search_api_get_info_view' => array(
      'title' => t('Retrieve information from a view'),
    ),
  );
}

/**
 * Helper function to add metatags.
 */
function caldeyro_services_search_api_add_metatag($is_node, $fields) {
  if ($is_node) {
    $location = '';
    $description = '';
    $keywords = '';
    if (!empty($fields['field_propiedad_ubicacion:name'])) {
      $location = $fields['field_propiedad_ubicacion:name'];
      $location = implode(', ', $location);
    }
    if (!empty($fields['field_descripcion_construcciones:value'])) {
      $description .= drupal_html_to_text($fields['field_descripcion_construcciones:value'][0]);
    }
    if (!empty($fields['field_descripcion_tierra:value'])) {
      $description .= drupal_html_to_text($fields['field_descripcion_tierra:value'][0]);
    }
    switch ($fields['type']) {
      case 'propiedad':
        $keywords = 'propiedad caldeyro, propiedad en venta uruguay, propiedad uruguay';
        if (!empty($fields['field_propiedad_categoria'])) {
          $keywords .= ', ' . $fields['field_propiedad_categoria'];
        }
        break;

      case 'complejo':
        $keywords = 'complejo caldeyro, complejo en venta uruguay, complejo uruguay';
        if (!empty($fields['field_categoria_complejo'])) {
          $keywords .= ', ' . $fields['field_categoria_complejo'];
        }
        break;
    }
    if (!empty($location)) {
      $keywords .= ', ' . $location;
    }
    $metatag = array(
      'Page title' => $fields['title'][0] . '-' . $location,
      'Description' => $description,
      'Keywords' => $keywords,
      'URL' => drupal_get_path_alias('node/' . $fields['nid'], $fields['language']),
    );
  }
  else {
    $metatag = array(
      'Page title' => 'Búsqueda de propiedades y complejos | ' . variable_get('site_name'),
      'Description' => variable_get('site_slogan'),
      'Keywords' => 'búsqueda, propiedades, complejos,  búsqueda de propiedades, búsqueda de complejos',
      'URL' => 'v2/busqueda',
    );
  }
  return $metatag;
}

/**
 * Helper function to translate items.
 */
function caldeyro_services_search_api_translate_items($facets, $lang){
  foreach ($facets as $key => &$facet) {
    switch ($key) {
      case 'field_vistas':
      case 'field_aptitud_o_potencial':
      case 'field_caract_grales_exteriores':
      case 'field_estado':
        $facet = caldeyro_services_facets_translate_items($facet, $key, $lang);
        break;

      case 'field_propiedad_categoria':
        if ($lang == 'en') {
          foreach ($facet as &$item) {
            $item['name'] = caldeyro_services_search_api_translate_term($item['filter'], $lang);
          }
        }
        break;
    }
  }
  return $facets;
}

/**
 * Helper function to group items if the wigdet is a 'search_api_ranges_ui_select'.
 */
function caldeyro_services_search_api_group_facets_items($index, &$facets) {
  foreach ($facets as $key => &$value) {
    $value = caldeyro_services_search_api_clean_double_quotes($value);
    switch ($key) {
      case 'field_precio_venta:amount':
      case 'field_precio_alquiler:amount':
      case 'search_api_aggregation_1':
      case 'search_api_aggregation_2':
      case 'field_banos':
      case 'field_dormitorios_totales':
      case 'field_indice_prod_promedio':
      case 'field_mconstruidos':
        $ranges = caldeyro_services_search_api_get_advanced_range($index, $key);
        $adv_ranges = _search_api_ranges_parse_advanced_range_settings($ranges);
        $value = caldeyro_services_search_api_map_values($adv_ranges, $value);
        break;
    }
  }
}

/**
 * Helper function to clean double quotes.
 */
function caldeyro_services_search_api_clean_double_quotes($values) {
  foreach ($values as $key => &$value) {
    $value['filter'] = str_replace('"', '', $value['filter']);
    if (is_numeric($value['filter'])) {
      $value['filter'] = (float) $value['filter'];
      if (empty($value['filter'])) {
        unset($values[$key]);
      }
    }
  }
  return $values;
}

/**
 * Helper function to map values in a range.
 */
function caldeyro_services_search_api_map_values($adv_ranges, $values) {
  if (count($adv_ranges)) {
    foreach ($adv_ranges as $range) {
      if (substr_count($range['value'], '-') == 1) {
        $maxmin = explode("-", $range['value']);
        $min = trim($maxmin[0]);
        $max = trim($maxmin[1]);
        $tmp_ranges[] = array(
          'min' => $min,
          'max' => $max,
          'label' => $range['label'],
        );
      }
    }
  }
  foreach ($tmp_ranges as $tmp_range_id => $tmp_range) {
    $min = (int) $tmp_range['min'];
    $max = (int) $tmp_range['max'];
    $result_count = 0;
    foreach ($values as $item) {
      $value = floatval($item['filter']);
      if ($value >= $min && $value <= $max) {
        $result_count += $item['count'];
        continue;
      }
      if ($value >= $min && ($max == '' || $max == '*')) {
        $result_count += $item['count'];
        continue;
      }
    }
    $adv_ranges[$tmp_range_id]['count'] = $result_count;
  }
  return $adv_ranges;
}

/**
 * Helper function to get enabled facets.
 */
function caldeyro_services_search_api_get_enabled_facets() {
  $caldeyro_facets = array(
    'field_propiedad_ubicacion' => array(),
    'field_propiedad_categoria' => array(),
    'field_vistas' => array(),
    'field_aptitud_o_potencial' => array(),
    'field_precio_venta:amount' => array(),
    'field_precio_alquiler:amount' => array(),
    'field_indice_prod_promedio' => array(),
    'field_mconstruidos' => array(),
    'field_dormitorios_totales' => array(),
    'field_banos' => array(),
    'field_caract_grales_exteriores' => array(),
    'search_api_aggregation_1' => array(),
    'search_api_aggregation_2' => array(),
    'field_estado' => array(),
  );
  return $caldeyro_facets;
}

/**
 * Helper function to get setting of a facet.
 */
function caldeyro_services_search_api_get_facet_setting($searcher, $facet_name) {
  $facet_settings = '';
  $query = db_select('facetapi', 'f');
  $query->fields('f', array('name', 'facet', 'settings', 'realm'));
  $query->condition('f.searcher', $searcher, '=');
  $query->condition('f.facet', $facet_name, '=');
  $result = $query->execute()->fetchAllAssoc('name');
  if (!empty($result)) {
    $facet_settings['key'] = $facet_name;
    foreach ($result as $key => $item) {
      $item->settings = unserialize($item->settings);
      if (empty($item->realm)) {
        $facet_settings['settings'] = $item->settings;
      }
      else {
        $facet_settings['widget'] = $item->settings['widget'];
        switch ($facet_settings['widget']) {
          case 'search_api_ranges_ui_select':
            $facet_settings['range'] = $item->settings['range_advanced'];
            break;
        }
      }
    }
  }
  return $facet_settings;
}

/**
 * Helper function to get setting of a facet.
 */
function caldeyro_services_search_api_get_advanced_range($index, $facet_name) {
  if (!$index) {
    return NULL;
  }
  $searcher = 'search_api@' . $index;
  $query = db_select('facetapi', 'f');
  $query->fields('f', array('name', 'facet', 'settings'));
  $query->condition('f.searcher', $searcher, '=');
  $query->condition('f.facet', $facet_name, '=');
  $query->condition('f.realm', 'block', '=');
  $result = $query->execute()->fetchAll();
  if (!empty($result)) {
    $result = reset($result);
    $settings = unserialize($result->settings);
    return $settings['range_advanced'];
  }
}

/**
 * Helper function to add counts item to terms.
 */
function caldeyro_services_search_api_get_init_terms_from_facet($index, $facet_name) {
  $filter['type'] = 'propiedad';
  $filter['language'] = 'es';
  $sort['created'] = 'DESC';
  $result = caldeyro_services_search_api_resource_retrieve($index, '', $filter, '', $sort, '', '', TRUE);
  $facet_items = $result['facets'][$facet_name];
  return $facet_items;
}

/**
 * Helper function to add counts item to terms.
 */
function caldeyro_services_search_api_add_search_api_counts(&$taxonomy_items, $facet_items) {
  foreach ($facet_items as $item) {
    $key = $item['filter'];
    if (isset($taxonomy_items[$key])) {
      $taxonomy_items[$key]->counts = $item['count'];
      continue;
    }
  }
}

/**
 * Helper function get items from a taxonomy.
 */
function caldeyro_services_search_api_get_items_from_taxonomy($taxonomy) {
  $vid = taxonomy_vocabulary_machine_name_load($taxonomy)->vid;
  $query = db_select('taxonomy_term_data', 'ttd');
  $query->join('taxonomy_term_hierarchy', 'tth', 'tth.tid = ttd.tid');
  $query->fields('ttd', array('tid', 'name'));
  $query->fields('tth', array('parent'));
  $query->condition('ttd.vid', $vid, '=');
  $result = $query->execute()->fetchAllAssoc('tid');
  return $result;
}

/**
 * Helper function to add a children to parent term.
 */
function caldeyro_services_search_api_add_children(&$result, $parent, $children) {
  if (isset($result[$parent])) {
    $result[$parent]->children[$children->tid] = $children;
    if (isset($result[$children->tid])) {
      unset($result[$children->tid]);
    }
    else {
      return $children->tid;
    }
    return;
  }
  foreach ($result as $term) {
    if (isset($term->children)) {
      $delete_term = caldeyro_services_search_api_add_children($term->children, $parent, $children);
      if (!isset($delete_term)) {
        unset($result[$delete_term]);
      }
      else {
        return $delete_term;
      }
    }
  }
}

/**
 * Determine whether the current user can access a submission resource.
 */
function caldeyro_search_api_node_resource_access($op, $args = array()) {
  return TRUE;
}

/**
 * Clear cache page.
 */
function caldeyro_services_search_api_node_update($node) {
  if ($node->type == 'propiedad' || $node->type == 'complejo') {
    $_SESSION['node_updated'][$node->nid] = TRUE;
  }
}

/**
 * Clear cache page.
 */
function caldeyro_services_search_api_node_postsave($node) {
  $nid = $node->nid;
  $node_path = $node->path['alias'];
  if (array_key_exists($nid, $_SESSION['node_updated'])) {
    $ref = field_get_items('node', $node, 'field_ref');
    $or = db_or();
    if (!empty($ref)) {
      $or->condition('cid', '%' . db_like($ref[0]['value']) . '%', 'LIKE');
    }
    $or->condition('cid', '%' . db_like($node_path) . '%', 'LIKE');
    $or->condition('cid', '%' . db_like($node->title) . '%', 'LIKE');
    $or->condition('cid', '%' . db_like($nid) . '%', 'LIKE');
    $node_deleted = db_delete('cache_page')
      ->condition($or)
      ->execute();
    // Clear field cache for the node.
    cache_clear_all('field:node:' . $nid, 'cache_field');
    // Reindex the node.
    search_api_track_item_change('node', array($nid));
    unset($_SESSION['node_updated'][$nid]);
  }
}
