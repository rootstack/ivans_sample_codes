<?php

/**
 * Implementation of hook_services_resources().
 */
function caldeyro_services_search_api_services_resources() {
  return array(
    'caldeyro_facets_search_api' => array(
      'retrieve' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'caldeyro_services_search_api',
          'name' => 'caldeyro_services_search_api.facets',
        ),
        'access arguments' => array('caldeyro_services_facets_api_get_facet_setting'),
        'callback' => 'caldeyro_search_api_get_facet',
        'args'     => array(
          array(
            'name' => 'index',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('path' => '0'),
            'description' => t('Search API index machine name to use.'),
          ),
          array(
            'name'         => 'facet_name',
            'type'         => 'string',
            'description'  => t('The facet name.'),
            'source'       => array('param' => 'facet_name'),
            'optional'     => TRUE,
            'default value' => 'all',
          ),
          array(
            'name'         => 'lang',
            'type'         => 'string',
            'description'  => t('The language of the search.'),
            'source'       => array('param' => 'lang'),
            'optional'     => FALSE,
            'default value' => 'es',
          ),
        ),
      ),
    ),
    'caldeyro_search_api' => array(
      'retrieve' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'caldeyro_services_search_api',
          'name' => 'caldeyro_services_search_api.resources',
        ),
        'callback' => 'caldeyro_services_search_api_resource_retrieve',
        'args' => array(
          array(
            'name' => 'index',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('path' => '0'),
            'description' => t('Search API index machine name to use.'),
          ),
          array(
            'name' => 'key',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'key'),
            'default value' => '',
            'description' => t('Search terms.'),
          ),
          array(
            'name' => 'filter',
            'type' => 'array',
            'optional' => TRUE,
            'source' => array('param' => 'filter'),
            'default value' => '',
            'description' => t('String of filters to apply to the search, in
              the form \'filter[~and][title]=title\'. Filters can be nested.'),
          ),
          array(
            'name' => 'fields',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'fields'),
            'description' => t('Field to use in the searches.'),
          ),
          array(
            'name' => 'sort',
            'type' => 'array',
            'optional' => TRUE,
            'source' => array('param' => 'sort'),
            'default value' => '',
            'description' => t('String containing the sort elements for the
              search in the form \'sort[nid]=DESC\'. Multiple sort terms can be used.'),
          ),
          array(
            'name' => 'items_per_page',
            'type' => 'int',
            'optional' => TRUE,
            'default value' => 10,
            'source' => array('param' => 'items_per_page'),
            'description' => t('Number of results to retrieve.'),
          ),
          array(
            'name' => 'page',
            'type' => 'int',
            'optional' => TRUE,
            'source' => array('param' => 'page'),
            'default value' => 0,
            'description' => t('The position of the first returned search result.'),
          ),
          array(
            'name' => 'facets',
            'type' => 'bool',
            'optional' => TRUE,
            'source' => array('param' => 'facets'),
            'description' => t('Search API only print facets'),
          ),
        ),
        'access arguments' => array('caldeyro_services_search_api_search_from_any_index'),
      ),
    ),
    'caldeyro_info_page' => array(
      'retrieve' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'caldeyro_services_search_api',
          'name' => 'caldeyro_services_search_api.page',
        ),
        'help' => t('Get a info page'),
        'callback' => 'caldeyro_services_search_api_get_page',
        'args' => array(
          array(
            'name' => 'index',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('path' => '0'),
            'description' => t('Search API index machine name to use.'),
          ),
          array(
            'name' => 'lang',
            'optional' => FALSE,
            'type' => 'string',
            'description' => t('The node language'),
            'default value' => 'es',
            'source' => array('param' => 'lang'),
          ),
          array(
            'name' => 'path_alias',
            'optional' => FALSE,
            'type' => 'string',
            'description' => t('The node path_alias'),
            'source' => array('param' => 'path_alias'),
          ),
        ),
        'access arguments' => array('caldeyro_services_search_api_get_info_page'),
      ),
    ),
    'caldeyro_menu' => array(
      'retrieve' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'caldeyro_services_search_api',
          'name' => 'caldeyro_services_search_api.menu',
        ),
        'help' => t('Get the menu items'),
        'callback' => 'caldeyro_services_search_api_get_menu',
        'args' => array(
          array(
            'name' => 'lang',
            'optional' => TRUE,
            'type' => 'string',
            'description' => t('The language of the menu'),
            'default value' => 'es',
            'source' => array('path' => 0),
          ),
          array(
            'name' => 'menu_name',
            'optional' => FALSE,
            'type' => 'string',
            'description' => t('The machine name of the menu to return'),
            'source' => array('param' => 'menu_name'),
          ),
        ),
        'access arguments' => array('caldeyro_services_search_api_get_info_menu'),
      ),
    ),
    'caldeyro_info_node' => array(
      'retrieve' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'caldeyro_services_search_api',
          'name' => 'caldeyro_services_search_api.node',
        ),
        'help' => t('Get a node info'),
        'callback' => 'caldeyro_services_search_api_get_node',
        'args' => array(
          array(
            'name' => 'index',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('path' => '0'),
            'description' => t('Search API index machine name to use.'),
          ),
          array(
            'name' => 'lang',
            'optional' => FALSE,
            'type' => 'string',
            'description' => t('The node language'),
            'default value' => 'es',
            'source' => array('param' => 'lang'),
          ),
          array(
            'name' => 'path_alias',
            'optional' => FALSE,
            'type' => 'string',
            'description' => t('The node path_alias'),
            'source' => array('param' => 'path_alias'),
          ),
          array(
            'name' => 'filter',
            'type' => 'array',
            'optional' => TRUE,
            'source' => array('param' => 'filter'),
            'default value' => '',
            'description' => t('String of filters to apply to the search, in
              the form \'filter[~and][title]=title\'. Filters can be nested.'),
          ),
        ),
        'access arguments' => array('caldeyro_services_search_api_get_info_node'),
      ),
    ),
    'caldeyro_total_result' => array(
      'retrieve' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'caldeyro_services_search_api',
          'name' => 'caldeyro_services_search_api.resources',
        ),
        'help' => t('Get the total items value'),
        'callback' => 'caldeyro_services_search_api_get_total_result',
        'args' => array(
          array(
            'name' => 'lang',
            'optional' => TRUE,
            'type' => 'string',
            'description' => t('The language of the menu'),
            'default value' => 'es',
            'source' => array('path' => 0),
          ),
        ),
        'access arguments' => array('caldeyro_services_search_api_search_from_any_index'),
      ),
    ),
    'caldeyro_most_viewed_properties' => array(
      'retrieve' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'caldeyro_services_search_api',
          'name' => 'caldeyro_services_search_api.view',
        ),
        'help' => t('Get a node info'),
        'callback' => 'caldeyro_services_search_api_get_most_viewed_properties',
        'args' => array(
          array(
            'name' => 'lang',
            'optional' => FALSE,
            'type' => 'string',
            'description' => t('The node language'),
            'default value' => 'es',
            'source' => array('path' => '0'),
          ),
          array(
            'name' => 'nid',
            'optional' => FALSE,
            'type' => 'int',
            'description' => t('The nid of the current node'),
            'source' => array('param' => 'nid'),
          ),
          array(
            'name' => 'category_property',
            'optional' => FALSE,
            'type' => 'int',
            'description' => t('The field_propiedad_categoria value of the current node'),
            'source' => array('param' => 'category_property'),
          ),
        ),
        'access arguments' => array('caldeyro_services_search_api_get_info_view'),
      ),
    ),
  );
}
