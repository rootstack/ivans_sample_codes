<?php

/**
 * Callback function for retrieve page info.
 */
function caldeyro_services_search_api_get_page($index, $lang, $path_alias) {
  module_load_include('inc', 'caldeyro_services_search_api', 'caldeyro_services_search_api.resources');
  $node = drupal_lookup_path('source', $path_alias, $lang);
  $arguments = explode('/', $node);
  if (!empty($arguments[1]) && is_numeric($arguments[1])) {
    $nid = $arguments[1];
    $node = node_load($nid);
    if (!empty($node) && $node->type == 'pagina') {
      $data = array();
      $node_wrapper = entity_metadata_wrapper('node', $node);
      $tnid = $node->tnid;
      $var = translation_node_get_translations($tnid);
      switch ($lang) {
        case 'es':
          $path_language = 'en';
          $path = 'node/' . $var[$path_language]->nid;
          break;

        case 'en':
          $path_language = 'es';
          $path = 'node/' . $var[$path_language]->nid;
          break;
      }
      $path = drupal_get_path_alias($path, $path_language);
      $data['translate'][$path_language] = $path;
      $data['title'] = $node_wrapper->title->value();
      $data['body'] = $node_wrapper->body->value();
      $image_static = $node_wrapper->field_imagen_pagina_estatica->value();
      $image_hero_fid = $node_wrapper->field_image_hero->value();
      $data['metatag'] = array(
        'Page title' => $data['title'] . ' | ' . variable_get('site_name', "Caldeyro Victorica"),
        'Description' => trim(str_replace(array("\n", "\t", "\r", "&nbsp;"), ' ', strip_tags($data['body']['value']))),
        'Keywords' => 'búsqueda, propiedades, complejos, búsqueda de propiedades, búsqueda de complejos',
      );
      if (!empty($image_hero_fid)) {
        $data['imageUrl'] = caldeyro_services_search_api_get_image_styles(array($image_hero_fid['fid']), $nid, 'field_image_hero');
      }
      if (!empty($image_static)) {
        $data['imageStaticUrl'] = caldeyro_services_search_api_get_image_styles(array($image_static['fid']), $nid, 'field_imagen_pagina_estatica');
      }
      return $data;
    }
  }
}
