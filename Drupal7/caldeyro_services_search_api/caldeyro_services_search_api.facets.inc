<?php

/**
 * Callback function for the index service call.
 */
function caldeyro_search_api_get_facet($index, $facet_name = '', $lang) {
  $searcher = 'search_api@' . $index;
  if (empty($facet_name)) {
    return t('The facet name is required.');
  }
  $caldeyro_facets = ($facet_name != 'all') ? array($facet_name => array()) : caldeyro_services_facets_get_enabled_facets();
  foreach ($caldeyro_facets as $key => $facet) {
    $caldeyro_facets[$key] = caldeyro_services_facets_get_facet_setting($searcher, $key);
    list($caldeyro_facets[$key]['title'], $caldeyro_facets[$key]['weight']) = caldeyro_services_facets_get_titles_and_weight($key);
    unset($caldeyro_facets[$key]['settings']['facet_mincount']);
    unset($caldeyro_facets[$key]['settings']['facet_missing']);
    unset($caldeyro_facets[$key]['settings']['flatten']);
    unset($caldeyro_facets[$key]['settings']['default_true']);
    unset($caldeyro_facets[$key]['settings']['facet_search_ids']);
    unset($caldeyro_facets[$key]['settings']['exclude']);
    unset($caldeyro_facets[$key]['settings']['hard_limit']);
    unset($caldeyro_facets[$key]['settings']['dependencies']);
    unset($caldeyro_facets[$key]['settings']['query_type']);
  }
  $facets['facets'] = caldeyro_services_facets_get_items_from_facet($index, $caldeyro_facets, $lang);
  return $facets;
}

/**
 * Callback function for add items to facet.
 */
function caldeyro_services_facets_get_items_from_facet($index, $facets, $lang) {
  foreach ($facets as $key => &$facet) {
    switch ($key) {
      case 'field_propiedad_ubicacion':
      case 'field_propiedad_categoria':
        if ($key == 'field_propiedad_ubicacion') {
          $taxonomy = 'tax_ubicacion';
        }
        if ($key == 'field_propiedad_categoria') {
          $taxonomy = 'categorias_propiedad';
        }
        $facet['items'] = caldeyro_services_facets_get_items_from_taxonomy($taxonomy);
        $facet_items = caldeyro_services_facets_get_init_terms_from_facet($index, $key, $lang);
        caldeyro_services_facets_add_search_api_counts($facet['items'], $facet_items);
        caldeyro_services_facets_sort_hierarchical_terms($facet['items']);
        if ($key == 'field_propiedad_categoria') {
          if ($lang == 'en') {
            $facet['items'] = caldeyro_services_facets_translate_terms($facet['items'], $lang);
          }
        }
        break;

      case 'field_vistas':
      case 'field_aptitud_o_potencial':
      case 'field_caract_grales_exteriores':
      case 'field_estado':
        $facet['items'] = caldeyro_services_facets_get_init_terms_from_facet($index, $key, $lang);
        $facet['items'] = caldeyro_services_facets_translate_items($facet['items'], $key, $lang);
        break;

      case 'field_propiedad_caract_extra':
        $field = 'field_propiedad_caract_extra';
        $facet['items'] = caldeyro_services_facets_get_items_from_list($field, $lang);
        break;

      case 'field_precio_venta:amount':
      case 'field_precio_alquiler:amount':
      case 'field_indice_prod_promedio':
      case 'field_mconstruidos':
      case 'field_dormitorios_totales':
      case 'field_banos':
      case 'search_api_aggregation_1':
      case 'search_api_aggregation_2':
        $facet['items'] = caldeyro_services_facets_get_init_terms_from_facet($index, $key, $lang);
        if (!empty($facet['items'])) {
          foreach ($facet['items'] as $key_item => &$item) {
            $item['weight'] = $key_item;
          }
        }
        break;
    }
  }
  return $facets;
}

/**
 * Helper function to get the facet title.
 */
function caldeyro_services_facets_get_titles_and_weight($facet_name) {
  switch ($facet_name) {
    case 'field_propiedad_ubicacion':
      $title_es = 'UBICACIÓN';
      $title_en = 'LOCATION';
      $weight = 0;
      break;

    case 'field_estado':
      $title_es = 'OPERACIÓN';
      $title_en = 'BUY OR RENT';
      $weight = 1;
      break;

    case 'field_propiedad_categoria':
      $title_es = 'TIPO DE PROPIEDAD';
      $title_en = 'PROPERTY TYPE';
      $weight = 2;
      break;

    case 'field_precio_venta:amount':
      $title_es = 'PRECIO VENTA (USD)';
      $title_en = 'SELLING PRICE (USD)';
      $weight = 3;
      break;

    case 'field_precio_alquiler:amount':
      $title_es = 'PRECIO ALQUILER (USD)';
      $title_en = 'RENT PRICE (USD)';
      $weight = 4;
      break;

    case 'field_indice_prod_promedio':
      $title_es = 'INDICE CO.N.E.A.T.';
      $title_en = 'CO.N.E.A.T. INDEX';
      $weight = 5;
      break;

    case 'search_api_aggregation_2':
      $title_es = 'HECTÁREAS';
      $title_en = 'HECTARES';
      $weight = 6;
      break;

    case 'search_api_aggregation_1':
      $title_es = 'SUPERFICIE DEL TERRENO (M²)';
      $title_en = 'LAND AREA(M²)';
      $weight = 7;
      break;

    case 'field_mconstruidos':
      $title_es = 'SUPERFICIE CONSTRUIDA (M²)';
      $title_en = 'LIVING AREA(M²)';
      $weight = 8;
      break;

    case 'field_dormitorios_totales':
      $title_es = 'DORMITORIOS';
      $title_en = 'BEDROOMS';
      $weight = 9;
      break;

    case 'field_banos':
      $title_es = 'BAÑOS';
      $title_en = 'BATHROOMS';
      $weight = 10;
      break;

    case 'field_aptitud_o_potencial':
      $title_es = 'APTITUD O POTENCIAL';
      $title_en = 'POTENTIAL OR CURRENT TYPE OF EXPLOITATION';
      $weight = 11;
      break;

    case 'field_caract_grales_exteriores':
      $title_es = 'FACILIDADES';
      $title_en = 'FACILITIES';
      $weight = 12;
      break;

    case 'field_vistas':
      $title_es = 'VISTAS';
      $title_en = 'VIEWS';
      $weight = 13;
      break;

    default:
      $title_es = $title_en = $facet_name;
      $weight = 0;
      break;
  }
  $title = array(
    'es' => $title_es,
    'en' => $title_en,
  );
  return array($title, $weight);
}

/**
 * Helper function to translate terms.
 */
function caldeyro_services_facets_translate_terms($items, $lang) {
  foreach ($items as $key => &$item) {
    $item->name = caldeyro_services_search_api_translate_term($item->tid, $lang);
  }
  return $items;
}

/**
 * Helper function to translate items.
 */
function caldeyro_services_facets_translate_items($items, $facet_name, $lang) {
  foreach ($items as &$item) {
    $string = 'field:' . $facet_name . ':#allowed_values:' . trim($item['filter']);
    $item['filter'] = i18n_string_translate($string, $item['filter'], array('langcode' => $lang));
  }
  return $items;
}

/**
 * Helper function get items from a taxonomy.
 */
function caldeyro_services_facets_get_items_from_taxonomy($taxonomy) {
  $vid = taxonomy_vocabulary_machine_name_load($taxonomy)->vid;
  $query = db_select('taxonomy_term_data', 'ttd');
  $query->join('taxonomy_term_hierarchy', 'tth', 'tth.tid = ttd.tid');
  $query->fields('ttd', array('tid', 'name'));
  $query->fields('tth', array('parent'));
  $query->condition('ttd.vid', $vid, '=');
  $result = $query->execute()->fetchAllAssoc('tid');
  return $result;
}

/**
 * Helper function to get the tid values of terms.
 */
function caldeyro_services_search_api_get_tids_from_taxonomy($taxonomy) {
  $vid = taxonomy_vocabulary_machine_name_load($taxonomy)->vid;
  $query = db_select('taxonomy_term_data', 'ttd');
  $query->fields('ttd', array('tid', 'name'));
  $query->condition('ttd.vid', $vid, '=');
  $result = $query->execute()->fetchAllAssoc('tid');
  $result = json_decode(json_encode($result), TRUE);
  return $result;
}

/**
 * Helper function to add empty terms.
 */
function caldeyro_services_search_api_add_empty_value_to_term($items) {
  $terms = array();
  foreach ($items as $key => &$item) {
    if ($key != 'field_propiedad_ubicacion' && $key != 'field_propiedad_categoria') {
      continue;
    }
    if ($key == 'field_propiedad_ubicacion') {
      $terms = caldeyro_services_search_api_get_tids_from_taxonomy('tax_ubicacion');
    }
    if ($key == 'field_propiedad_categoria') {
      $terms = caldeyro_services_search_api_get_tids_from_taxonomy('categorias_propiedad');
    }
    if (!empty($terms)) {
      foreach ($terms as $tid => $term) {
        $validation = FALSE;
        foreach ($item as $value) {
          if ($value['filter'] == $tid) {
            $validation = TRUE;
          }
        }
        if (!$validation) {
          $item[] = array('filter' => "$tid", 'count' => 0);
        }
      }
    }
  }
  return $items;
}

/**
 * Helper function to add counts item to terms.
 */
function caldeyro_services_facets_get_init_terms_from_facet($index, $facet_name, $lang) {
  module_load_include('inc', 'caldeyro_services_search_api', 'caldeyro_services_search_api.resources');
  $filter['type'] = 'propiedad';
  $filter['language'] = $lang;
  $sort['created'] = 'DESC';
  $result = caldeyro_services_search_api_resource_retrieve($index, '', $filter, '', $sort, 1, 0, TRUE);
  $facet_items = $result['facets'][$facet_name];
  return $facet_items;
}

/**
 * Helper function to add counts item to terms.
 */
function caldeyro_services_facets_add_search_api_counts(&$taxonomy_items, $facet_items) {
  foreach ($facet_items as $item) {
    $key = $item['filter'];
    if (isset($taxonomy_items[$key])) {
      $taxonomy_items[$key]->counts = $item['count'];
      continue;
    }
  }
}

/**
 * Helper function to sort hierarchically a list of terms.
 */
function caldeyro_services_facets_sort_hierarchical_terms(&$result) {
  foreach ($result as $key => $term) {
    $parent = $term->parent;
    if ($parent) {
      $delete_term = caldeyro_services_search_api_add_children($result, $parent, $result[$key]);
      if ($delete_term) {
        unset($result[$delete_term]);
      }
    }
  }
}

/**
 * Helper function to get enabled facets.
 */
function caldeyro_services_facets_get_enabled_facets() {
  $caldeyro_facets = array(
    'field_propiedad_ubicacion' => array(),
    'field_propiedad_categoria' => array(),
    'field_vistas' => array(),
    'field_aptitud_o_potencial' => array(),
    'field_precio_venta:amount' => array(),
    'field_precio_alquiler:amount' => array(),
    'field_indice_prod_promedio' => array(),
    'field_mconstruidos' => array(),
    'field_dormitorios_totales' => array(),
    'field_banos' => array(),
    'field_caract_grales_exteriores' => array(),
    'search_api_aggregation_1' => array(),
    'search_api_aggregation_2' => array(),
    'field_estado' => array(),
  );
  return $caldeyro_facets;
}

/**
 * Helper function to get setting of a facet.
 */
function caldeyro_services_facets_get_facet_setting($searcher, $facet_name) {
  $facet_settings = '';
  $query = db_select('facetapi', 'f');
  $query->fields('f', array('name', 'facet', 'settings', 'realm'));
  $query->condition('f.searcher', $searcher, '=');
  $query->condition('f.facet', $facet_name, '=');
  $result = $query->execute()->fetchAllAssoc('name');
  if (!empty($result)) {
    $facet_settings['key'] = $facet_name;
    foreach ($result as $key => $item) {
      $item->settings = unserialize($item->settings);
      if (empty($item->realm)) {
        $facet_settings['settings'] = $item->settings;
      }
      else {
        $facet_settings['widget'] = $item->settings['widget'];
        switch ($facet_settings['widget']) {
          case 'search_api_ranges_ui_select':
            $facet_settings['range'] = $item->settings['range_advanced'];
            break;

        }
      }
    }
  }
  return $facet_settings;
}

/**
 * Helper function to sort hierarchically a list of terms.
 */
function caldeyro_services_search_api_sort_hierarchical_terms(&$result) {
  foreach ($result as $key => $term) {
    $parent = $term->parent;
    if ($parent) {
      $delete_term = caldeyro_services_search_api_add_children($result, $parent, $result[$key]);
      if ($delete_term) {
        unset($result[$delete_term]);
      }
    }
  }
}

/**
 * Helper function to get items from a specific field.
 */
function caldeyro_services_facets_get_items_from_list($field, $lang) {
  $field = field_info_field($field);
  $allowed_values = i18n_field_translate_allowed_values($field, $lang);
  return $allowed_values;
}
