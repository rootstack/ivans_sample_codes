<?php

/**
 * Callback function to return the total result of a searcb.
 */
function caldeyro_services_search_api_get_total_result($lang) {
  return array('total' => variable_get('total_main_search_value_' . $lang));
}

/**
 * Helper function to init filters.
 */
function caldeyro_services_search_api_get_initial_filters($filter) {
  if (!empty($filter['nid'])) {
    return $filter;
  }
  $filter['language'] = (!empty($filter['language'])) ? $filter['language'] : 'es';
  $filter['type'] = (!empty($filter['type'])) ? $filter['type'] : 'propiedad';
  $filter['status'] = (!empty($filter['status'])) ? $filter['status'] : 1;
  if ($filter['type'] == 'propiedad') {
    $filter['field_propiedad_categoria'] = (!empty($filter['field_propiedad_categoria'])) ? $filter['field_propiedad_categoria'] : '22,15,11,7,19,33,25,21,20,31,24,8,17,27,12,16,26';
    $filter['field_estado'] = (!empty($filter['field_estado'])) ? $filter['field_estado'] : 'En venta, En alquiler';
  }
  return $filter;
}

/**
 * Callback function for the index service call.
 */
function caldeyro_services_search_api_resource_retrieve($index, $key = '', $filter, $fields, $sort, $items_per_page, $page, $facets = FALSE) {
  $options = array(
    'parse mode' => 'terms',
    'limit' => $items_per_page,
    'offset' => $page * $items_per_page,
  );
  $filter = caldeyro_services_search_api_get_initial_filters($filter);
  $related_property = FALSE;
  if ($filter['type'] == 'complejo') {
    $related_property = TRUE;
  }
  $query = search_api_query($index, $options);
  if (!empty($key)) {
    $query->keys($key);
    if (!empty($fields)) {
      $fields = explode(",", $fields);
      foreach ($fields as $field) {
        $fields_filter[$field] = $field;
      }
    }
    $fields_filter['title'] = 'title';
    $fields_filter['field_ref'] = 'field_ref';
    $fields_filter['field_descripcion_construcciones:value'] = 'field_descripcion_construcciones:value';
    $fields_filter['field_descripcion_tierra:value'] = 'field_descripcion_tierra:value';
    $query->fields($fields_filter);
  }

  $lang = $filter['language'];
  $nid = (!empty($filter['nid'])) ? $filter['nid'] : '';
  $is_node = (!empty($nid)) ? TRUE : FALSE;

  // Set the filters, transforming the query array in a SearchAPIQueryFilter.
  $tmp_filter = array();
  foreach ($filter as $facet_key => $value) {
    $tmp_key = (strpos($facet_key, 'field_') !== FALSE || strpos($facet_key, 'search_api_') !== FALSE) ? $facet_key : 'basic';
    if ($facet_key == 'field_precio_venta:amount' || $facet_key == 'field_precio_alquiler:amount'
      || $facet_key == 'search_api_aggregation_1' || $facet_key == 'search_api_aggregation_2'
      || $facet_key == 'field_banos' || $facet_key == 'field_dormitorios_totales'
      || $facet_key == 'field_indice_prod_promedio' || $facet_key == 'field_mconstruidos') {

      $min_max = explode("-", $value);
      $min = trim($min_max[0]);
      $max = trim($min_max[1]);
      if ($min == $max) {
        $tmp_filter[$tmp_key]['~and'][] = array(
          'field' => $facet_key,
          'value' => floatval($min),
          'op' => '=',
        );
      }
      else {
        $tmp_filter[$tmp_key]['~and'][] = array(
          'field' => $facet_key,
          'value' => floatval($min),
          'op' => '>=',
        );
        if (!empty($max)) {
          $tmp_filter[$tmp_key]['~and'][] = array(
            'field' => $facet_key,
            'value' => floatval($max),
            'op' => '<=',
          );
        }
      }
      continue;
    }
    $pos = strpos($value, ',');
    $tmp_oper = '~and';
    if ($pos === FALSE) {
      if ($tmp_key == 'field_propiedad_ubicacion' || $tmp_key == 'field_propiedad_categoria') {
        $tmp_oper = '~or';
      }
      $tmp_filter[$tmp_key][$tmp_oper][] = array(
        'field' => $facet_key,
        'value' => trim($value),
        'op' => '=',
      );
    }
    else {
      $value = explode(",", $value);
      foreach ($value as $item) {
        if ($facet_key == 'field_vistas' || $facet_key == 'field_caract_grales_exteriores' || $facet_key == 'field_propiedad_caract_extra' ||
          $facet_key == 'field_precio_alquiler:amount' || $facet_key == 'search_api_aggregation_2' || $facet_key == 'field_precio_venta:amount' ||
          $facet_key == 'search_api_aggregation_1' || $facet_key == 'field_banos' || $facet_key == 'field_dormitorios_totales' ||
          $facet_key == 'field_indice_prod_promedio' || $facet_key == 'field_mconstruidos') {
          $op = '~and';
        }
        else {
          $op = '~or';
        }
        $tmp_filter[$tmp_key][$op][] = array(
          'field' => $facet_key,
          'value' => trim($item),
          'op' => '=',
        );
      }
    }
  }
  $filter = $tmp_filter;
  if (!empty($filter) && is_array($filter)) {
    $query_filter = new SearchApiQueryFilter();
    caldeyro_services_search_api_add_filter($filter, $query_filter);
    $query->filter($query_filter);
  }

  if (!empty($sort) && is_array($sort)) {
    foreach ($sort as $field => $mode) {
      $query->sort($field, $mode);
    }
  }
  $result = $query->execute();
  if ($result) {
    $fields = !empty($result['results'][$nid]['fields']) ? $result['results'][$nid]['fields'] : '';
    $metatag = caldeyro_services_search_api_add_metatag($is_node, $fields);
  }
  $return = array();
  $return['facets'] = caldeyro_services_search_api_filter_enabled_facets($result['search_api_facets']);
  caldeyro_services_search_api_group_facets_items($index, $return['facets']);
  caldeyro_services_search_api_clean_empty_values($return['facets']);
  caldeyro_services_search_api_alter_facet_items($return['facets'], $lang);

  $return['facets'] = caldeyro_services_search_api_translate_items($return['facets'], $lang);
//  $return['facets'] = caldeyro_services_search_api_add_empty_value_to_term($return['facets']);
  if ($facets) {
    return $return;
  }
  else {
    $return['metatag'] = !empty($metatag) ? $metatag : '';
    $return['count'] = $result['result count'];
    $return['data'] = array_values($result['results']);
    foreach ($return['data'] as &$node) {
      $node['fields']['pdf'] = $lang . '/pdf/' . $node['fields']['nid'];
      $node['fields']['path_alias'] = !empty($metatag) ? $return['metatag']['URL'] : '';
      if (!empty($node['fields']['field_prop_tipo_proveedor'])) {
        unset($node['fields']['field_prop_tipo_proveedor']);
      }
      if (!empty($node['fields']['field_proveedor:name'])) {
        unset($node['fields']['field_proveedor:name']);
      }
      if (!empty($node['fields']['field_proveedor:sugar_id'])) {
        unset($node['fields']['field_proveedor:sugar_id']);
      }
      if ($lang == 'en') {
        if (!empty($node['fields']['field_estado'])) {
          $node['fields']['field_estado'] = caldeyro_services_search_api_translate_field($node['fields']['field_estado'], 'field_estado', $lang);
        }
        if (!empty($node['fields']['field_vistas'])) {
          $node['fields']['field_vistas'] = caldeyro_services_search_api_translate_field($node['fields']['field_vistas'], 'field_vistas', $lang);
        }
        if (!empty($node['fields']['field_tipo_de_instalaciones'])) {
          $node['fields']['field_tipo_de_instalaciones'] = caldeyro_services_search_api_translate_field($node['fields']['field_tipo_de_instalaciones'], 'field_tipo_de_instalaciones', $lang);
        }
        if (!empty($node['fields']['field_instalaciones_extras'])) {
          $node['fields']['field_instalaciones_extras'] = caldeyro_services_search_api_translate_field($node['fields']['field_instalaciones_extras'], 'field_instalaciones_extras', $lang);
        }
        if (!empty($node['fields']['field_prop_aguada_tipo_nombre'])) {
          $node['fields']['field_prop_aguada_tipo_nombre'] = caldeyro_services_search_api_translate_field($node['fields']['field_prop_aguada_tipo_nombre'], 'field_prop_aguada_tipo_nombre', $lang);
        }
        if (!empty($node['fields']['field_tipo_terreno_campo'])) {
          $node['fields']['field_tipo_terreno_campo'] = caldeyro_services_search_api_translate_field($node['fields']['field_tipo_terreno_campo'], 'field_tipo_terreno_campo', $lang);
        }
        if (!empty($node['fields']['field_tipo_ute'])) {
          $node['fields']['field_tipo_ute'] = caldeyro_services_search_api_translate_field($node['fields']['field_tipo_ute'], 'field_tipo_ute', $lang);
        }
        if (!empty($node['fields']['field_propiedad_categoria'])) {
          foreach ($node['fields']['field_propiedad_categoria'] as $key => $item) {
            $node['fields']['field_propiedad_categoria:name'][$key] = caldeyro_services_search_api_translate_term($item, $lang);
          }
        }
      }
      $node['fields']['more_features'] = array();
      $node['fields']['more_features'][0] = '';

      if (!empty($node['fields']['field_cantidad_potreros'])) {
        $node['fields']['more_features'][0] .= t('Divided into @count paddocks', array(
            '@count' => $node['fields']['field_cantidad_potreros'],
          )
        ) . ', ';
      }
      if (!empty($node['fields']['field_estado_del_alambrado'])) {
        $node['fields']['more_features'][0] .= ucfirst(strtolower(t('@status wiring state', array(
            '@status' => strtolower($node['fields']['field_estado_del_alambrado']),
          )
        ) . ' &#8226; '));
      }
      if (!empty($node['fields']['field_tipo_de_instalaciones'])) {
        $node['fields']['more_features'][0] .= strtolower(implode(", ", $node['fields']['field_tipo_de_instalaciones'])) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_estado_instalaciones'])) {
        $node['fields']['more_features'][0] .= t('Status of the installations') . ' ' . strtolower($node['fields']['field_estado_instalaciones']) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_hectareas_montes'][0]) && !empty($node['fields']['field_tipo_montes'][0])) {
        $node['fields']['more_features'][0] .= t('@count ha of mountain @type', array(
            '@count' => $node['fields']['field_hectareas_montes'][0],
            '@type' => strtolower($node['fields']['field_tipo_montes']),
          )
        ) . ', ';
      }
      if (!empty($node['fields']['field_arboles:field_tipo_arbol'])) {
        $node['fields']['more_features'][0] .= strtolower(implode($node['fields']['field_arboles:field_tipo_arbol'])) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_numero_galpones'])) {
        $shed_string = t('shed');
        if ($node['fields']['field_numero_galpones'] > 0) {
          $shed_string = t('sheds');
        }
        $node['fields']['more_features'][0] .= t('@count @shed,', array(
            '@count' => $node['fields']['field_numero_galpones'],
            '@shed' => $shed_string,
          )
        ) . ' ';
      }
      if (!empty($node['fields']['field_estado_galpones'])) {
        $node['fields']['more_features'][0] .= t('status @status,', array(
            '@status' => strtolower($node['fields']['field_estado_galpones']),
          )
        ) . ' ';
      }
      if (!empty($node['fields']['field_superficie_total'])) {
        $node['fields']['more_features'][0] .= t('it covers a total area of @total_area m<sup>2</sup>', array(
            '@total_area' => $node['fields']['field_superficie_total'],
          )
        ) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_instalaciones_extras'])) {
        $node['fields']['more_features'][0] .= strtolower(implode(", ", $node['fields']['field_instalaciones_extras'])) . ' &#8226; ';
      }

      if (!empty($node['fields']['field_propiedad_caract_extra'])) {
        $node['fields']['more_features'][0] .= strtolower(implode(", ", $node['fields']['field_propiedad_caract_extra'])) . ' &#8226; ';
      }
      if (empty($node['fields']['more_features'][0])) {
        unset($node['fields']['more_features'][0]);
        $key_features = 0;
      }
      else {
        $key_features = 1;
      }
      $node['fields']['more_features'][$key_features] = '';
      if (!empty($node['fields']['field_tipo_de_terreno'])) {
        $node['fields']['more_features'][$key_features] .= t('Ground') . ' ' . strtolower($node['fields']['field_tipo_de_terreno']) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_caract_grales_prop'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst((implode(", ", $node['fields']['field_caract_grales_prop']))) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_pisos'])) {
        $node['fields']['more_features'][$key_features] .= t('Floors @floor', array(
            '@floor' => strtolower(implode(", ", $node['fields']['field_pisos'])),
          )
        ) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_paredes'])) {
        $str_wall = (!empty($node['fields']['field_pisos'])) ? ' ' . t('walls') : t('Walls');
        $node['fields']['more_features'][$key_features] .= t('@str_wall @wall', array(
            '@str_wall' => $str_wall,
            '@wall' => strtolower(implode(", ", $node['fields']['field_paredes'])),
          )
        ) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_cerramientos'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst(strtolower(implode(", ", $node['fields']['field_cerramientos']))) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_prop_enfriamiento'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst(strtolower(implode(", ", $node['fields']['field_prop_enfriamiento']))) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_calefaccion'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst(strtolower(implode(", ", $node['fields']['field_calefaccion']))) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_calef_ctral_tipo'])) {
        $node['fields']['more_features'][$key_features] .= t('Central heating by') . ' ' . strtolower($node['fields']['field_calef_ctral_tipo']) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_conexiones_previstas'])) {
        $node['fields']['more_features'][$key_features] .= t('Connections provided') . ': ' . strtolower(implode(", ", $node['fields']['field_conexiones_previstas'])) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_incluye_extra'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst(strtolower(implode(", ", $node['fields']['field_incluye_extra']))) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_caract_grales_exteriores'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst(strtolower(implode(", ", $node['fields']['field_caract_grales_exteriores']))) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_fachadas'])) {
        $node['fields']['more_features'][$key_features] .= t('Facade @facade,', array(
            '@facade' => strtolower(implode(", ", $node['fields']['field_fachadas'])),
          )
        ) . ' ';
      }
      if (!empty($node['fields']['field_techo'])) {
        $str_roof = (!empty($node['fields']['field_fachadas'])) ? ' ' . t('roof') : t('Roof');
        $node['fields']['more_features'][$key_features] .= t('@str_roof @roof', array(
            '@str_roof' => $str_roof,
            '@roof' => strtolower(implode(", ", $node['fields']['field_techo'])),
          )
        ) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_aguas'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst(strtolower(implode(", ", $node['fields']['field_aguas']))) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_seguridad'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst(strtolower(implode(", ", $node['fields']['field_seguridad']))) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_vistas'])) {
        $node['fields']['more_features'][$key_features] .= t('Views to') . ' ' . strtolower(implode(", ", $node['fields']['field_vistas'])) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_zona_geografica'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst(strtolower(implode(", ", $node['fields']['field_zona_geografica']))) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_aptitud_o_potencial'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst(strtolower(implode(", ", $node['fields']['field_aptitud_o_potencial']))) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_relevante_const'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst(strtolower(implode(", ", $node['fields']['field_relevante_const']))) . ' &#8226; ';
      }
      if (!empty($node['fields']['field_tipo_de_construccion'])) {
        $node['fields']['more_features'][$key_features] .= ucfirst(strtolower(implode(", ", $node['fields']['field_tipo_de_construccion']))) . ' &#8226; ';
      }
      if (empty($node['fields']['more_features'][$key_features])) {
        unset($node['fields']['more_features'][$key_features]);
      }
      $node['fields']['observations'] = '';
      if (!empty($node['fields']['field_des_tierra_observaciones'][0])) {
        $node['fields']['observations'][] = $node['fields']['field_des_tierra_observaciones'][0] . ' ';
      }
      if (!empty($node['fields']['field_prop_const_observaciones'][0])) {
        $node['fields']['observations'][] = $node['fields']['field_prop_const_observaciones'][0] . ' ';
      }
      if (!empty($node['fields']['field_imgs:file:fid'])) {
        $node['fields']['imageUrl'] = caldeyro_services_search_api_get_image_styles($node['fields']['field_imgs:file:fid'], $node['fields']['nid'], 'field_imgs');
      }
      $node['fields']['field_node_link'] = drupal_get_path_alias('node/' . $node['fields']['nid'], $lang);
      if (!empty($node['fields']['field_complejo_nombre_comp'])) {
        $node['fields']['field_complejo_link'] = drupal_get_path_alias('node/' . $node['fields']['field_complejo_nombre_comp'], $lang);
        $node['fields']['field_complejo_nombre_com'] = caldeyro_services_search_get_title($node['fields']['field_complejo_nombre_comp']);
      }
    }
    if ($related_property) {
      module_load_include('inc', 'caldeyro_services_search_api', 'caldeyro_services_search_api.resources');
      $view_name = 'caldeyro_v2_complejo_unidades_relacionadas';
      $display_id = 'default';
      $result = views_get_view_result($view_name, $display_id, $nid);
      $return['data']['property_related'] = array();
      foreach ($result as $item) {
        $nid = $item->node_field_data_field_unidades_asociadas_nid;
        $filter['nid'] = $nid;
        $items_per_page = 1;
        $page = 0;
        module_load_include('inc', 'caldeyro_services_search_api', 'caldeyro_services_search_api.resources');
        $data = caldeyro_services_search_api_resource_retrieve($index, '', $filter, '', '', $items_per_page, $page, FALSE);
        $return['data']['property_related'][] = $data['data'];
      }

    }
  }
  return $return;
}

/**
 * Helper function to translate term.
 */
function caldeyro_services_search_api_translate_term($tid, $lang) {
  $term = i18n_string_translate(
    'taxonomy:term:' . $tid . ':name',
    '',
    array('langcode' => $lang));
  return $term;
}

/**
 * Helper function to translate item.
 */
function caldeyro_services_search_api_translate_field($field, $facet_name, $lang) {
  if (is_array($field)) {
    foreach ($field as &$item) {
      $string = 'field:' . $facet_name . ':#allowed_values:' . trim($item);
      $item = i18n_string_translate($string, $item, array('langcode' => $lang));
    }
  }
  else {
    $string = 'field:' . $facet_name . ':#allowed_values:' . trim($field);
    $field = i18n_string_translate($string, $field, array('langcode' => $lang));
  }
  return $field;
}

/**
 * Helper function to get the different image styles.
 */
function caldeyro_services_search_api_get_image_styles($images, $nid, $field_name) {
  $image_styles = array(
    'xs' => 'caldeyro_xs',
    'md' => 'caldeyro_md',
    'lg' => 'caldeyro_lg',
    'xlg' => 'caldeyro_xlg',
  );
  $temp = array();
  $key = 0;
  foreach ($images as $key_img => $fid) {
    if (!empty($nid) && !empty($field_name)) {
      $visibility = caldeyro_services_search_get_visibility_status($nid, $key_img, $field_name);
      if ($visibility == 0) {
        continue;
      }
    }
    $temp[$key] = array();
    foreach ($image_styles as $key_style => $style) {
      $image_uri = caldeyro_services_search_api_get_uri_from_fid($fid);
      $derivative_uri = image_style_path($style, $image_uri);
      $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $image_uri, $derivative_uri);
      if ($success) {
        $temp[$key][$key_style] = file_create_url($derivative_uri);
      }
      else {
        $temp[$key][$key_style] = '';
      }
    }
    $key++;
  }
  return $temp;
}

/**
 * Helper function get uri from a image.
 */
function caldeyro_services_search_api_get_uri_from_fid($fid) {
  $query = db_select('file_managed', 'fm');
  $query->fields('fm', array('uri'));
  $query->condition('fm.fid', $fid, '=');
  return $query->execute()->fetchField();
}

/**
 * Helper function to alter items from the facets.
 */
function caldeyro_services_search_api_alter_facet_items(&$results, $lang) {
  if (!empty($results['field_estado'])) {
    foreach ($results['field_estado'] as $key_item => $item) {
      switch($item['filter']) {
        case "en venta":
          $results['field_estado'][$key_item]['label'] = ($lang == 'es') ? 'Venta' : 'Sale';
          break;
        case 'en alquiler':
          $results['field_estado'][$key_item]['label'] = ($lang == 'es') ? 'Alquiler' : 'Rent';
          break;
      }
    }
  }
}

/**
 * Helper function to clean empty values.
 */
function caldeyro_services_search_api_clean_empty_values(&$results) {
  foreach ($results as $key_facet => $facet) {
    foreach ($facet as $key_item => $item) {
      if ($key_facet == 'field_estado') {
        if ($item['filter'] != 'En venta' && $item['filter'] != 'En alquiler' &&
          $item['filter'] != 'en venta' && $item['filter'] != 'en alquiler') {
          unset($results[$key_facet][$key_item]);
          continue;
        }
      }
      if ($item['count'] == 0) {
        unset($results[$key_facet][$key_item]);
        continue;
      }
    }
  }
}

/**
 * Helper function to filter facets enables.
 */
function caldeyro_services_search_api_filter_enabled_facets($facets) {
  module_load_include('inc', 'caldeyro_services_search_api', 'caldeyro_services_search_api.facets');
  $caldeyro_facets = caldeyro_services_facets_get_enabled_facets();
  $facet_result = array();
  foreach ($caldeyro_facets as $key => $facet) {
    $facet_result[$key] = (array_key_exists($key, $facets)) ? $facets[$key] : array();
  }
  return $facet_result;
}

/**
 * Prepare the query filters for the Search API query object.
 */
function caldeyro_services_search_api_add_filter(array $filters, &$query_filter) {
  foreach ($filters as $key_filter => $filter) {
    if (empty($filter['~and']) && empty($filter['~or'])) {
      $filter = array('~and' => $filter);
    }
    foreach ($filter as $key => $value) {
      if (is_array($value) && !empty($value)) {
        $conjunction = 'AND';
        if ($key == '~or') {
          $conjunction = 'OR';
        }
        $tag = array();
        if ($key_filter == 'field_propiedad_ubicacion' || $key_filter == 'field_propiedad_categoria') {
          $tag = array('facet:' . $key_filter);
        }
        $process_filter = new SearchApiQueryFilter($conjunction, $tag);
        foreach ($value as $k => $v) {
          $field_name = $v['field'];
          $process_filter->condition($field_name, $v['value'], $v['op']);
        }

        if (!empty($query_filter) && $query_filter instanceof SearchApiQueryFilter) {
          $query_filter->filter($process_filter);
        }
        else {
          $query_filter = $process_filter;
        }
      }
    }
  }
}

/**
 * Helper function to get title from a node.
 */
function caldeyro_services_search_get_title($nid) {
  $title = "";
  if ($nid) {
    $title = db_select('node', 'r')
      ->fields('r', array('title'))
      ->condition('nid', strtolower($nid), '=')
      ->execute()->fetchField();
  }
  return $title;
}

/**
 * Helper function to get the visibility status of a image.
 */
function caldeyro_services_search_get_visibility_status($nid, $delta, $field_name = NULL) {
  $query = db_select('field_image_field_visibility', 'v');
  $query->fields('v', array('visibility'));
  $query->condition('v.entity_id', $nid, '=');
  $query->condition('v.delta', $delta, '=');
  if (!empty($field_name)) {
    $query->condition('v.field_name', $field_name, '=');
  }
  return $query->execute()->fetchField();
}
