<?php

/**
 * Callback function for retrieve the list of most viewed properties.
 */
function caldeyro_services_search_api_get_most_viewed_properties($lang = 'es', $nid, $category_property) {
  module_load_include('inc', 'caldeyro_services_search_api', 'caldeyro_services_search_api.resources');
  $view_name = 'nodo_relacionadas_prop';
  $display_id = 'block_1';
  $result = views_get_view_result($view_name, $display_id, $lang, $nid, $category_property);
  $data = array();
  foreach ($result as $item) {
    $images = field_get_items('node', $item->_field_data['field_propiedad_categoria_taxonomy_term_data_nid_1']['entity'], 'field_imgs');
    $fid = '';
    foreach ($images as $img) {
      if ($img['image_field_visibility'] == 1) {
        $fid = $img['fid'];
        break;
      }
    }
    $data[] = array(
      'title' => $item->field_propiedad_categoria_taxonomy_term_data_title,
      'image_url' => caldeyro_services_search_api_get_image_styles(array($fid), '', ''),
      'path' => drupal_get_path_alias('node/' . $item->field_propiedad_categoria_taxonomy_term_data_nid, $lang),
    );
  }
  return $data;
}
